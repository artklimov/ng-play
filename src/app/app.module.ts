import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { NgModule, ApplicationRef } from '@angular/core';
import { RouterModule } from '@angular/router';

import 'hammerjs';

/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { MerchantComponent } from './merchant/merchant.component';
import { BankAccountsComponent } from './merchant/bank-accounts/bank-accounts.component';
import { PaymentPointsComponent } from './merchant/payment-points/payment-points.component';

import * as services from './services';

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    SearchComponent,
    MerchantComponent,
    BankAccountsComponent,
    PaymentPointsComponent
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    ENV_PROVIDERS,
    services.SearchService,
    services.MerchantService,
    services.BankAccountsService,
    services.FilesService,
    services.LegalEntitiesService,
    services.LegalEntityFilesService,
    services.PaymentPointsService
  ]
})
export class AppModule {
}
