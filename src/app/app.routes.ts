import { Routes } from '@angular/router';
import { MerchantComponent } from './merchant/merchant.component';

import { DataResolver } from './app.resolver';

export const ROUTES: Routes = [
  { path: 'merchants/:id', component: MerchantComponent }
];
