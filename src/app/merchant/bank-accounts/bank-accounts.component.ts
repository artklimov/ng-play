import { Component, Input } from '@angular/core';
import { BankAccountsService } from '../../services';
import { BankAccount } from '../../models/merchant/merchant';

@Component({
    selector: 'bank-accounts',
    templateUrl: 'bank-accounts.component.html',
    styleUrls: ['bank-accounts.component.css']
})
export class BankAccountsComponent {
    public bankAccounts: BankAccount[];
    public errorMessage: string;
    public newAccount: string;

    private _merchantId: string;

    constructor(private _service: BankAccountsService) {}

    @Input()
    public set merchantId(value: string) {
        this._merchantId = value;
        this.onMerchantChanged(value);
    }

    public add() {
        this.errorMessage = null;
        this._service
            .post(this._merchantId, this.newAccount)
            .subscribe(
                (d) => {
                    this.bankAccounts.push(d);
                },
                (e) => this.errorMessage = e);
        this.newAccount = '';
    }

    public delete(bandAccount: BankAccount) {
        this.errorMessage = null;
        this._service
            .delete(this._merchantId, bandAccount.id)
            .subscribe(
                (d) => {
                    let index = this.bankAccounts.indexOf(bandAccount);
                    this.bankAccounts.splice(index, 1);
                },
                (e) => this.errorMessage = e
            );
    }

    private onMerchantChanged(merchantId: string) {
        this._service.get(merchantId).subscribe(
            (d) => this.bankAccounts = d,
            (e) => this.errorMessage = e
        );
    }
}
