import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { MerchantService } from '../services/merchant.service';
import { Merchant } from '../models/merchant/merchant';

@Component({
    selector: 'merchant',
    templateUrl: 'merchant.component.html',
    styleUrls: ['merchant.component.css']
})
export class MerchantComponent implements OnInit, OnDestroy {
    public merchant: Merchant;

    private _getMerchantSubscription: Subscription;

    constructor(private _route: ActivatedRoute, private _merchantService: MerchantService) { }

    public ngOnInit() {
        this._getMerchantSubscription = this._route.params
            .switchMap((params: Params) => this._merchantService.get(params['id']))
            .subscribe((merchant) => this.merchant = merchant);
    }

    public ngOnDestroy() {
        this._getMerchantSubscription.unsubscribe();
    }
 }
