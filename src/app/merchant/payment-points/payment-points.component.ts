import { Component, Input } from '@angular/core';
import { PaymentPointsService } from '../../services';
import { BankAccountsService } from '../../services';
import { PaymentPoint } from '../../models/merchant/merchant';
import { BankAccount } from '../../models/merchant/merchant';

@Component({
    selector: 'payment-points',
    templateUrl: 'payment-points.component.html',
    styleUrls: ['payment-points.component.css']
})

export class PaymentPointsComponent {
    public paymentPoints: PaymentPoint[];
    public bankAccounts: BankAccount[];
    public errorMessage: string;
    public newPaymentPoint: string;
    public newPaymentPointBankAccount: string;

    private _merchantId: string;

    constructor(
        private _paymentPointsService: PaymentPointsService,
        private _bankAccountsService: BankAccountsService) {}

    @Input()
    public set merchantId(value: string) {
        this._merchantId = value;
        this.onMerchantChanged(value);
    }

    public add() {
        this.errorMessage = null;
        this._paymentPointsService
            .post(this._merchantId, this.newPaymentPoint, this.newPaymentPointBankAccount)
            .subscribe(
                (d) => {
                    this.paymentPoints.push(d);
                    this.newPaymentPoint = '';
                    this.newPaymentPointBankAccount = '';
                },
                (e) => this.errorMessage = e);
    }

    public delete(paymentPoint: PaymentPoint) {
        this.errorMessage = null;
        console.log(paymentPoint.id);
        this._paymentPointsService
            .delete(this._merchantId, paymentPoint.id)
            .subscribe(
                (d) => {
                    let index = this.paymentPoints.indexOf(paymentPoint);
                    this.paymentPoints.splice(index, 1);
                },
                (e) => this.errorMessage = e
            );
    }

    private onMerchantChanged(merchantId: string) {
        this._paymentPointsService.get(merchantId).subscribe(
            (d) => this.paymentPoints = d,
            (e) => this.errorMessage = e
        );
        this._bankAccountsService.get(merchantId).subscribe(
            (d) => this.bankAccounts = d,
            (e) => this.errorMessage = e
        );

    }
}
