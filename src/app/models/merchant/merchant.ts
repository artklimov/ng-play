export interface Merchant {
    id: string;
    name: string;
    address: string;
    cvr: string;
}

export interface PaymentPoint {
    id: string;
    number: string;
    bankAccountNumber: string;
}

export interface BankAccount {
    id: string;
    number: string;
}

export interface LegalEntity {
    id: string;
    name: string;
}

export interface File {
    id: string;
    name: string;
    contentType: string;
}

export interface FileCollection {
    tag: string;
    files: File[];
}
