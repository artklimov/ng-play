import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';

import { SearchService } from '../services/search.service';
import { Merchant } from '../models/merchant/merchant';

@Component({
    selector: 'search',
    templateUrl: 'search.component.html',
    styleUrls: ['search.component.css']
})
export class SearchComponent {
    public merchants: Merchant[] = [];
    public errorMessage: string;

    private inputChange: Subject<string>;

    constructor(
            private _searchService: SearchService,
            private _router: Router) {
        this.inputChange = new Subject<string>();
        this.inputChange
            .distinctUntilChanged()
            .debounceTime(500)
            .do((_) => this.merchants = [])
            .switchMap((i) => this._searchService.find(i))
            .subscribe(
                (d) => this.merchants = d,
                (e) => this.errorMessage = e);
    }

    public onSearchTermChange(e) {
        this.inputChange.next(e);
    }

    public selectMerchant(merchant: Merchant) {
        this._router.navigate(['/merchants', merchant.id]);
        this.merchants = [];
    }
 }
