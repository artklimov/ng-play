import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BankAccount } from '../models/merchant/merchant';
import { baseUrl } from './resources';

@Injectable()
export class BankAccountsService {

    constructor(private _http: Http) {}

    public get(merchantId): Observable<BankAccount[]> {
        return this._http
            .get(`${baseUrl}/merchants/${merchantId}/bank-accounts`)
            .map((r) => r.json() as BankAccount[]);
    }

    public post(merchantId: string, accountNumber: string): Observable<BankAccount> {
        let body = {
            number: accountNumber
        };
        return this._http
            .post(`${baseUrl}/merchants/${merchantId}/bank-accounts`, body)
            .map((r) => r.json() as BankAccount);
    }

    public delete(merchantId: string, acccountId: string): Observable<boolean> {
        return this._http
            .delete(`${baseUrl}/merchants/${merchantId}/bank-accounts/${acccountId}`)
            .map((r) => true);
    }
}
