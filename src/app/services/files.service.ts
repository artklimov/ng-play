import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { FileCollection } from '../models/merchant/merchant';
import { baseUrl } from './resources';

@Injectable()
export class FilesService {

    constructor(private _http: Http) {}

    public get(merchantId): Observable<FileCollection[]> {
        return this._http
            .get(`${baseUrl}/merchants/${merchantId}/files`)
            .map((r) => r.json() as FileCollection[]);
    }
}
