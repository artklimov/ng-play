import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { LegalEntity } from '../models/merchant/merchant';
import { baseUrl } from './resources';

@Injectable()
export class LegalEntitiesService {

    constructor(private _http: Http) {}

    public get(merchantId): Observable<LegalEntity[]> {
        return this._http
            .get(`${baseUrl}/merchants/${merchantId}/legal-entities`)
            .map((r) => r.json() as LegalEntity[]);
    }
}
