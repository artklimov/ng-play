import { Injectable } from '@angular/core';
import {  Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { Merchant } from '../models/merchant/merchant';
import { baseUrl } from './resources';

@Injectable()
export class MerchantService {
    constructor(private _http: Http) { }

    public get(id: string): Observable<Merchant> {
        return this._http
            .get(`${baseUrl}/merchants/${id}`)
            .map((x) => x.json());
    }
}
