import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { PaymentPoint } from '../models/merchant/merchant';
import { baseUrl } from './resources';

@Injectable()
export class PaymentPointsService {

    constructor(private _http: Http) {}

    public get(merchantId): Observable<PaymentPoint[]> {
        return this._http
            .get(`${baseUrl}/merchants/${merchantId}/payment-points`)
            .map((r) => r.json() as PaymentPoint[]);
    }

    public post(merchantId: string, ppNumber: string, baNumber: string):
        Observable<PaymentPoint> {
        let body = {
            number: ppNumber,
            bankAccountNumber: baNumber
        };
        return this._http
            .post(`${baseUrl}/merchants/${merchantId}/payment-points`, body)
            .map((r) => r.json() as PaymentPoint);
    }

    public delete(merchantId: string, paymentPointId: string): Observable<boolean> {
        return this._http
            .delete(`${baseUrl}/merchants/${merchantId}/payment-points/${paymentPointId}`)
            .map((r) => true);
    }
}
