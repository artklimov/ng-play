import { Injectable } from '@angular/core';
import {  Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { Merchant } from '../models/merchant/merchant';
import { baseUrl } from './resources';

@Injectable()
export class SearchService {
    constructor(private _http: Http) { }

    public find(term: string): Observable<Merchant[]> {
        return this._http
            .get(`${baseUrl}/merchants?searchTerm=${term}`)
            .map((x) => x.json());
    }
}
